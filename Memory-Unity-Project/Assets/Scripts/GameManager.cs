﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class GameManager : MonoBehaviour
{
    public static int CurrentLevelNumber;

    [System.Flags]
    public enum Symbols
    {
        Waves = 1 << 0,
        Dot = 1 << 1,
        Square = 1 << 2,
        LargeDiamond = 1 << 3,
        SmallDiamonds = 1 << 4,
        Command = 1 << 5,
        Bomb = 1 << 6,
        Sun = 1 << 7,
        Bones = 1 << 8,
        Drop = 1 << 9,
        Face = 1 << 10,
        Hand = 1 << 11,
        Flag = 1 << 12,
        Disk = 1 << 13,
        Candle = 1 << 14,
        Wheel = 1 << 15
    }

    [System.Serializable]
    public struct LevelDescription
    {
        public int Rows, Columns;
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    public GameObject TilePrefab;
    public float TileSpacing;
    float numberofmistakes = 0;
    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;
    public float playTime;
    private Tile m_tileOne, m_tileTwo;
    private int m_cardsRemaining;

    private void Start()
    {
        LoadLevel(CurrentLevelNumber);
    }
    void PlayTimeTimer()
    {
        playTime += Time.deltaTime;
    }

    //This function loads in the level.
    //It uses an int level number as a parameter and then reads an array of levels and chooses one. 
    //It then fetches a list of symbols. 



    private void LoadLevel(int levelNumber)
    {
        LevelDescription level = Levels[levelNumber % Levels.Length];

        List<Symbols> symbols = GetRequiredSymbols(level);

        //For each loop of rows  and columns and sets the ypos, checks the rows index and multiplies the row index by the spacing of the tiles. 
        //does the same for the xpos, and creates a tile prefab at each of those positions. 
        //randomizes a symbol, and gets the symbols material, and applies a texture to the randomized symbol. 
        //Takes the symbols index and removes it from the list and keeps going, making sure it doesnt reset the symbol. 
        //sets camera to correct pos.

        for (int rowIndex = 0; rowIndex < level.Rows; ++rowIndex)
        {
            float yPosition = rowIndex * (1 + TileSpacing);
            for (int colIndex = 0; colIndex < level.Columns; ++colIndex)
            {
                float xPosition = colIndex * (1 + TileSpacing);
                GameObject tileObject = Instantiate(TilePrefab, new Vector3(xPosition, yPosition, 0), Quaternion.identity, this.transform);
                int symbolIndex = UnityEngine.Random.Range(0, symbols.Count);
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol(symbols[symbolIndex]);
                tileObject.GetComponent<Tile>().Initialize(this, symbols[symbolIndex]);
                symbols.RemoveAt(symbolIndex);
            }
        }

        SetupCamera(level);
    }
    void Update()
    {
        PlayTimeTimer();


    }

    //Gets the symbols needed by passing the level and getting a list of symbols used in the level. 
    private List<Symbols> GetRequiredSymbols(LevelDescription level)
    {
        List<Symbols> symbols = new List<Symbols>();
        int cardTotal = level.Rows * level.Columns;

        //creates an array of all the symbols you have and stores them in the array. 
        //Checks remaining cards left against the card total. If it's not even throws an error. 
        {
            Array allSymbols = Enum.GetValues(typeof(Symbols));

            m_cardsRemaining = cardTotal;

            if (cardTotal % 2 > 0)
            {
                new ArgumentException("There must be an even number of cards");
            }

            foreach (Symbols symbol in allSymbols)
            {
                if ((level.UsedSymbols & symbol) > 0)
                {
                    symbols.Add(symbol);
                }
            }
        }

        //If there are no symbols in the list, throws an error. If there are too many symbols for the number of cards/2, throws an error. 
        {
            if (symbols.Count == 0)
            {
                new ArgumentException("The level has no symbols set");
            }
            if (symbols.Count > cardTotal / 2)
            {
                new ArgumentException("There are too many symbols for the number of cards.");
            }
        }

        //Gets a repeat count and is made up of the card total /2 minus total number of symbols. 
        //creates a list of symbols and stores it. Also clears the duplicate symbols list. 
        //For loop that adds to the duplicate symbols and removes from the symbolcopy list using the same random index.
        //if the symbols copy is 0, adds a new range of symbols for the next time it loops. 
        {
            int repeatCount = (cardTotal / 2) - symbols.Count;
            if (repeatCount > 0)
            {
                List<Symbols> symbolsCopy = new List<Symbols>(symbols);
                List<Symbols> duplicateSymbols = new List<Symbols>();
                for (int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex)
                {
                    int randomIndex = UnityEngine.Random.Range(0, symbolsCopy.Count);
                    duplicateSymbols.Add(symbolsCopy[randomIndex]);
                    symbolsCopy.RemoveAt(randomIndex);
                    if (symbolsCopy.Count == 0)
                    {
                        symbolsCopy.AddRange(symbols);
                    }
                }
                symbols.AddRange(duplicateSymbols);
            }
        }

        symbols.AddRange(symbols);

        return symbols;
    }

    //This calculates the offset for each symbol. Reads the symbol array and gets a value corresponding to the symbol in the list.
    //Returns a vector2 with it's offset using the rows and columns. If it fails to find a symbol returns 0. 
    private Vector2 GetOffsetFromSymbol(Symbols symbol)
    {
        Array symbols = Enum.GetValues(typeof(Symbols));
        for (int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex)
        {
            if ((Symbols)symbols.GetValue(symbolIndex) == symbol)
            {
                return new Vector2(symbolIndex % 4, symbolIndex / 4) / 4f;
            }
        }
        Debug.Log("Failed to find symbol");
        return Vector2.zero;
    }

    //Sets the camera to a specific orthographic size depending on the amount of rows and columns.  This also takes into account the tile spacing.
    private void SetupCamera(LevelDescription level)
    {
        Camera.main.orthographicSize = (level.Rows + (level.Rows + 1) * TileSpacing) / 2;
        Camera.main.transform.position = new Vector3((level.Columns * (1 + TileSpacing)) / 2, (level.Rows * (1 + TileSpacing)) / 2, -10);
    }

    //This starts coroutines to start the flipping animation. It takes a tile and reveals it, if theres no second tile selected it reveals itself. If its a match it passes it on. 
    public void TileSelected(Tile tile)
    {
        if (m_tileOne == null)
        {
            m_tileOne = tile;
            m_tileOne.Reveal();
        }
        else if (m_tileTwo == null)
        {
            m_tileTwo = tile;
            m_tileTwo.Reveal();
            if (m_tileOne.Symbol == m_tileTwo.Symbol)
            {
                StartCoroutine(WaitForHide(true, 1f));
            }
            else
            {
                StartCoroutine(WaitForHide(false, 1f));
                numberofmistakes++;




            }
        }
    }

    //Once the level is complete it increments the current level number. It checks if the current level number is as big as the levels length, if it is the game is over. 
    //If its smaller then it loads the next level. 
    private void LevelComplete()
    {
        ++CurrentLevelNumber;
        if (CurrentLevelNumber > Levels.Length - 1)
        {
            Debug.Log("GameOver");
            Analytics.CustomEvent("WinState", new Dictionary<string, object>
        {
            { "Game Win on level", CurrentLevelNumber}

});

            Analytics.CustomEvent("Game over timer", new Dictionary<string, object>
        {
            { "playtime" , playTime }



});
        }
        else
        {

            Analytics.CustomEvent("Mistakes on certain level", new Dictionary<string, object>
        {
            { "Number of mistakes per level", numberofmistakes},
                {"level", CurrentLevelNumber-1 }

});
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    void OnApplicationQuit()
    {
        

        Analytics.CustomEvent("Level close", new Dictionary<string, object>
        {
            { "Level they closed on", CurrentLevelNumber

}

});
    }
    //This is a coroutine which waits, checks for a match and then flips it. If its a match it destorys both objects and reduces the cards remaining int. 
    //Completes the level when there are no more cards. 
    private IEnumerator WaitForHide(bool match, float time)
    {
        float timer = 0;
        while (timer < time)
        {
            timer += Time.deltaTime;
            if (timer >= time)
            {
                break;
            }
            yield return null;
        }
        if (match)
        {
            Destroy(m_tileOne.gameObject);
            Destroy(m_tileTwo.gameObject);
            m_cardsRemaining -= 2;
        }
        else
        {
            m_tileOne.Hide();
            m_tileTwo.Hide();
        }
        m_tileOne = null;
        m_tileTwo = null;

        if (m_cardsRemaining == 0)
        {
            LevelComplete();
        }
    }
}
