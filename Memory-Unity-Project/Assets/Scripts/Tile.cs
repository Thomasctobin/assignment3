﻿using UnityEngine;
using System.Collections;
using UnityEngine.Analytics;
using System.Collections.Generic;
public class Tile : MonoBehaviour
{
    private GameManager m_manager;
    public GameManager.Symbols Symbol { get; private set; }
    private Transform m_child;
    private Collider m_collider;
    float playTime = 0;
    float numberofclicks = 0;
    //On initialize it is setting the M_child from the initial child. 
    //It is also getting access to the collider class. 
    private void Start()
    {
        m_child = this.transform.GetChild(0);
        m_collider = GetComponent<Collider>();
    }

    //This allows the manager to determine which tile is being selected. 
    private void OnMouseDown()
    {
        m_manager.TileSelected(this);
        numberofclicks++;
    }

    void Update()
    {
        PlayTimeTimer();


    }
    
    void PlayTimeTimer()
    {
        playTime += Time.deltaTime;
    }



    void OnApplicationQuit()
    {

        AnalyticsEvent.GameOver();
        Analytics.CustomEvent("gameOver", new Dictionary<string, object>
        {
            { "playtime" , playTime },
            {"clickcount",numberofclicks }
});
    
    }



    //This initializes takes the game manager and symbol and stores it to each tile.
    public void Initialize( GameManager manager, GameManager.Symbols symbol )
    {
        m_manager = manager;
        Symbol = symbol;
    }

    //This starts the coroutine that spins the pieces by 180 degrees, over .8secs. 
    //This also stops all existing coroutines to play this animation. Also disables its current collider. 
    public void Reveal()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 180, 0.8f ) );
        m_collider.enabled = false;
    }

    //This also stops all existing coroutines to play this animation.Also enables its current collider.
    public void Hide()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 0, 0.8f ) );
        m_collider.enabled = true;
    }

    //This is the spinning part. Stores the initial tile rotation and its child euler angles. 
    //Syncs delta time so it spins with real time. 
    //Uses lerp spin to spin it smoothy. 
    //sets rotation back to initial. 
    private IEnumerator Spin( float target, float time )
    {
        float timer = 0;
        float startingRotation = m_child.eulerAngles.y;
        Vector3 euler = m_child.eulerAngles;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            euler.y = Mathf.LerpAngle( startingRotation, target, timer / time );
            m_child.eulerAngles = euler;
            yield return null;
        }
        euler.y = target;
        m_child.eulerAngles = euler;
    }
}
